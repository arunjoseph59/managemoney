//
//  CardDataModel.swift
//  Manage$
//
//  Created by A-10474 on 23/02/22.
//

import Foundation

struct CardDataModel: Identifiable, Hashable {
    var id: Int
    var cardNumber: String
    var availableAmount: String
    var currencySymbol: String
    var expiry: String
}

extension CardDataModel {
    
    static func getCardData()-> [CardDataModel] {
        return [
            CardDataModel(id: 1, cardNumber: "1234 7434 0678 9087", availableAmount: "459", currencySymbol: "$", expiry: "20/2025"),
            CardDataModel(id: 2, cardNumber: "4234 5464 5378 0787", availableAmount: "674", currencySymbol: "$", expiry: "14/2024"),
            CardDataModel(id: 3, cardNumber: "6786 4389 4578 0924", availableAmount: "890", currencySymbol: "$", expiry: "03/2027")
        ]
    }
}
