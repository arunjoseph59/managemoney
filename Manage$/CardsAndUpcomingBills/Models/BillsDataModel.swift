//
//  BillsDataModel.swift
//  Manage$
//
//  Created by A-10474 on 23/02/22.
//

import Foundation

struct BillsDataModel: Hashable, Identifiable {
    
    var id: Int
    var item: String
    var image: String
    var amount: String
    var date: String
}

extension BillsDataModel {
    static func getBills()-> [BillsDataModel] {
        return [
            BillsDataModel(id: 1, item: "Netflix", image: "netflix", amount: "$23.4", date: "12/03/2022"),
            BillsDataModel(id: 2, item: "Spotify", image: "spotify", amount: "$14.4", date: "22/04/2022"),
            BillsDataModel(id: 3, item: "Tinder", image: "tinder", amount: "$12.4", date: "05/03/2022")
        ]
    }
}
