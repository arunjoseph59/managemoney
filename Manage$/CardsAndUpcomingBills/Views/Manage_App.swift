//
//  Manage_App.swift
//  Manage$
//
//  Created by A-10474 on 23/02/22.
//

import SwiftUI

@main
struct Manage_App: App {
    var body: some Scene {
        WindowGroup {
            CardsAndUpcomingBillsView()
        }
    }
}
