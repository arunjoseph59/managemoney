//
//  CardsView.swift
//  Manage$
//
//  Created by A-10474 on 23/02/22.
//

import SwiftUI

struct CardsView: View {
    var body: some View {
        
        ZStack {
            RoundedRectangle(cornerRadius: 20.0, style: .continuous)
                .fill(Color.blue)
                .frame(width: 300.0, height: 200.0, alignment: .center)
            
            VStack(spacing: 60.0) {
                HStack {
                    VStack(alignment: .leading, spacing: 0) {
                        Image("man")
                            .resizable()
                            .frame(width: 30.0, height: 30.0, alignment: .center)
                        Text("**** **** **** 2345")
                            .foregroundColor(.white)
                            .font(.system(size: 16.0, weight: .semibold))
                    }
                    
                    Spacer()
                }
                
                HStack {
                    VStack(alignment: .leading, spacing: 5) {
                        Text("$786.90")
                            .foregroundColor(.white)
                            .font(.system(size: 20.0, weight: .bold))
                        Text("Exp 04/2025")
                            .foregroundColor(.white)
                            .font(.system(size: 15.0, weight: .semibold))
                    }
                    
                    Spacer()
                }
            }
            .padding(.horizontal)
        }
    }
}

struct CardsView_Previews: PreviewProvider {
    static var previews: some View {
        CardsView()
    }
}
