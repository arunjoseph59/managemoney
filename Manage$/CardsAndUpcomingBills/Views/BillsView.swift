//
//  BillsView.swift
//  Manage$
//
//  Created by A-10474 on 23/02/22.
//

import SwiftUI

struct BillsView: View {
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 15.0, style: .continuous)
                .fill(Color.violet)
                .frame(width: 200.0, height: 250.0, alignment: .center)
            
            VStack(alignment: .leading ,spacing: 60.0) {
                HStack {
                    VStack(alignment: .leading, spacing: 2.0) {
                        Text("Spotify")
                            .foregroundColor(.white)
                            .font(.system(size: 16.0))
                    Image("")
                        .frame(width: 30.0, height: 30.0, alignment: .center)
                    }
                    Spacer()
                }
                            
                HStack {
                    VStack(alignment: .leading, spacing: 5.0) {
                        Text("$17.45")
                            .foregroundColor(.white)
                            .font(.system(size: 25.0, weight: .heavy))
                    Text("23/03/2022")
                        .foregroundColor(.white)
                        .font(.system(size: 16.0))
                    }
                    
                    Spacer()
                }
            }
            .padding(.horizontal)
        }
    }
}

struct BillsView_Previews: PreviewProvider {
    static var previews: some View {
        BillsView()
    }
}
