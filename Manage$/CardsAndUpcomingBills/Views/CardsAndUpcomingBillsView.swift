//
//  ContentView.swift
//  Manage$
//
//  Created by A-10474 on 23/02/22.
//

import SwiftUI

struct CardsAndUpcomingBillsView: View {
    
    var cards: [CardDataModel] = CardDataModel.getCardData()
    var bills: [BillsDataModel] = BillsDataModel.getBills()
    
    var body: some View {
        
        ZStack {
            Color.offWhite
                .edgesIgnoringSafeArea(.all)                
            VStack(alignment: .leading) {
                HStack {
                    Spacer()
                    Image("man")
                        .resizable()
                        .frame(width: 80.0, height: 80, alignment: .center)
                        .cornerRadius(40.0)
                        .background(Color.lightBlue.cornerRadius(40.0))
                }
                
                Spacer()
                    .frame(height: 40.0)
                
                HStack {
                    VStack(alignment: .leading, spacing: 5.0) {
                        Text("Good morning")
                            .foregroundColor(Color.gray)
                            .font(.system(size: 14.0))
                        Text("Daniel Morris")
                            .foregroundColor(.black)
                            .font(.system(size: 18.0, weight: .heavy))
                    }
                    Spacer()
                }
                
                Spacer()
                    .frame(height: 40.0)
                
                ScrollView(.horizontal) {
                    HStack {
                        ForEach (-1..<cards.count) { index in
                            switch index {
                            case -1: addCardView
                            default: CardsView()
                            }
                        }
                    }
                }
                
                Spacer()
                    .frame(height: 40.0)
                
                Text("Upcoming Bills")
                    .foregroundColor(.black)
                    .font(.system(size: 18.0, weight: .heavy))
                
                Spacer()
                    .frame(height: 20.0)
                
                ScrollView(.horizontal) {
                    HStack {
                        ForEach(bills.indices) { index in
                            BillsView()
                        }
                    }
                }
                    
                Spacer()
            }
            .padding()
        }
    }
    
    var addCardView: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 20.0, style: .continuous)
                .fill(Color.blue)
                .frame(width: 70.0, height: 200.0, alignment: .center)
            
            Image(systemName: "plus")
                .font(.system(size: 20.0, weight: .heavy))
                .foregroundColor(.white)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        CardsAndUpcomingBillsView()
    }
}
