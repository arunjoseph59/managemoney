//
//  Color+Extensions.swift
//  Manage$
//
//  Created by A-10474 on 23/02/22.
//

import Foundation
import SwiftUI

extension Color {
    
    public static var offWhite: Color {
        return Color(UIColor(red: 242/255, green: 246/255, blue: 254/255, alpha: 1.0))
    }
    
    public static var lightBlue: Color {
        return Color(UIColor(red: 224/255, green: 233/255, blue: 244/255, alpha: 1.0))
    }
    
    public static var violet: Color {
        return Color(UIColor(red: 111/255, green: 93/255, blue: 231/255, alpha: 1.0))

    }
}
